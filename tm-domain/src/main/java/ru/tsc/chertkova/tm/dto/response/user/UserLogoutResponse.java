package ru.tsc.chertkova.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsc.chertkova.tm.dto.response.user.AbstractUserResponse;

@Getter
@Setter
@NoArgsConstructor
public final class UserLogoutResponse extends AbstractUserResponse {
}
